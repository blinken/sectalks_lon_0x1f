#!/bin/bash

if ! [ -x /usr/bin/qemu-system-x86_64 ]; then
  echo "No QEMU x86_64 system emulator found. On Ubuntu, try installing the 'qemu-system-x86' package."
  exit 1
fi

echo To exit qemu type Ctrl-A then C, then type 'quit'
sleep 2

qemu-system-x86_64 \
  -bios bios.bin \
  -kernel vmlinuz-4.15.0-43-generic \
  -append "console=ttyS0" \
  -initrd initrd.gz \
  -nographic \
  -nodefaults \
  -device virtio-serial-pci \
  -serial mon:stdio \
  -m 512
